all: build

build:
	cargo build --release

install:
	cargo install

clean:
	cargo clean

.PHONY: all build install clean
