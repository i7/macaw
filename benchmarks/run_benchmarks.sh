#!/bin/bash

#time limit (4 hours)
timelimit_hours=4
timelimit=$(($timelimit_hours * 60 * 60))

#memory limit (32 gigabyte)
memlimit_gb=2
mem_soft=$(($memlimit_gb * 1024 * 1024))
mem_hard=$(($mem_soft + 1024))

macawexe="../target/release/macaw"
teddexe="tedd"
lolaexe="lola"
pntoolsdir="$(realpath ~/local/pn_suite)"
pntoolsexe="pn_tools"

outdir="out"

function check_soundness {
    indir=$1
    resultsfile=$2
    echo "file concurrency places transitions acyclic markedgraph" >$resultsfile
    for file in $(find $indir -mindepth 1 -type f -name "*.pnml"); do
        mkdir -p ${outdir}/$(dirname $file)
        outfile=${outdir}/$file.soundness

        $macawexe --soundness --info --markoutputs --disregardoutputs --upper --natural $file >$outfile 2>&1
        result=$?
        if [[ $result -eq 0 ]]; then
            places=$(sed -e '/^Number of places: *\([0-9]*\)$/!D' -e 's/^Number of places: *\([0-9]*\)$/\1/' $outfile)
            transitions=$(sed -e '/^Number of transitions:.*\([0-9]*\)$/!D' -e 's/^Number of transitions: *\([0-9]*\)$/\1/' $outfile)
            acyclic=$(sed -e '/^Acyclic: *\(\w*\)$/!D' -e 's/^Acyclic: *\(\w*\)$/\1/' $outfile)
            markedgraph=$(sed -e '/^Marked graph: *\(\w*\)$/!D' -e 's/^Marked graph: *\(\w*\)$/\1/' $outfile)
            concurrency=$(sed -e '/^Upper bound on concurrency threshold: *\([0-9]*\)$/!D' -e 's/^Upper bound on concurrency threshold: *\([0-9]*\)$/\1/' $outfile)
            echo "$file $concurrency $places $transitions $acyclic $markedgraph" >>$resultsfile
            echo -n "."
        else
            echo -n "x"
        fi
    done
    echo
}

function convert_nets {
    indir=$1
    for file in $(find $indir -mindepth 1 -type f -name "*.pnml"); do
        outputdir=$(dirname $file)/output
        outfile=$outputdir/bpel-net.lola
        newfile=${file%.pnml}.lola

        #echo "Converting $file"
        fullpath=$(realpath $file)
        pushd $pntoolsdir >/dev/null
        echo -e "2\n4\n" | ./$pntoolsexe $fullpath >/dev/null
        popd >/dev/null

        mv $outfile $newfile
        rm -r $outputdir

        echo -n "."
    done
    echo
}

function adapt_nets {
    indir=$1
    for file in $(find $indir -mindepth 1 -type f -name "*.pnml"); do
        # add page tag
        sed -e 's/\(<net[^>]*>\)/\1<page id="page1">/' -e 's/\(<\/net>\)/<\/page>\1/' -i $file
        echo -n "."
    done
    for file in $(find $indir -mindepth 1 -type f -name "*.lola"); do
        # add page tag
        #sed -e 's/\(<net[^>]*>\)/\1<page id="page1">/' -e 's/\(<\/net>\)/<\/page>\1/' -i $file
        echo -n "."
    done
    echo
}

function compute_upper_macaw {
    infile=$1
    resultsfile=$2
    var_type=$3
    if [ "$var_type" == "int" ]; then
        options="--natural"
    else
        options=""
    fi
    {
        read header
        echo "$header upper_${var_type} time_upper_${var_type}" >$resultsfile
        while read file info; do

            mkdir -p ${outdir}/$(dirname $file)
            outfile=${outdir}/$file.upper_${var_type}
            lpfile=${outdir}/$file.upper_${var_type}.lp

            timing="$(date +%s%N)"
            (
                set -o pipefail
                ulimit -S -v $mem_soft
                ulimit -H -v $mem_hard
                timeout $timelimit $macawexe --markoutputs --disregardoutputs --upper -o $lpfile $options $file >$outfile 2>&1
            ) 2>/dev/null
            result=$?
            timing=$(($(date +%s%N)-timing))
            if [[ $result -eq 0 ]]; then
                upper=$(sed -e '/^Upper bound on concurrency threshold: *\([0-9]*\)$/!D' -e 's/^Upper bound on concurrency threshold: *\([0-9]*\)$/\1/' $outfile)
                echo -n "."
            elif [[ $result -eq 124 ]] || [[ $result -eq 137 ]]; then
                upper='timeout'
                echo -n "x"
            else
                upper='error'
                echo -n "x"
            fi
            echo "$file $info $upper $timing" >>$resultsfile
        done
    } < $infile
    echo
}

function compute_lower_lola {
    infile=$1
    resultsfile=$2
    {
        read header
        echo "$header lower_lola time_lower_lola" >$resultsfile
        while read file concurrency info; do
            lolafile=${file%.pnml}.lola

            mkdir -p ${outdir}/$(dirname $file)
            outfile=${outdir}/$file.lower_lola

            formula=${outdir}/$lolafile.lower.task
            k=$concurrency

            echo "REACHABLE (" >$formula
            sed -e '1,/PLACE/d' -e '/;/,$d' -e 's/,/ +/g' $lolafile >>$formula
            finalplaces=$(grep -A 1 "<finalmarking" $file | sed -e 's/\s*<text>\s*//g' -e 's/\s*<\/text>\s*//' -e '2p;d')
            for p in $finalplaces; do
                placename=$(grep -A 2 "id=\"$p\"" $file | sed -e 's/\s*<text>\s*//g' -e 's/\s*<\/text>\s*//' -e '3p;d')
                echo " - $placename" >>$formula
            done
            echo ">= $k)" >>$formula

            timing="$(date +%s%N)"
            (
                set -o pipefail
                ulimit -S -v $mem_soft
                ulimit -H -v $mem_hard
                timeout $timelimit $lolaexe -f $formula $lolafile >$outfile 2>&1
            ) 2>/dev/null
            result=$?
            timing=$(($(date +%s%N)-timing))
            if [[ $result -eq 0 ]]; then
                if grep -q "lola: *result: *yes" $outfile; then
                    lower="$k"
                elif grep -q "lola: *result: *no" $outfile; then
                    lower="<$k"
                else
                    lower='unknown'
                fi
                echo -n "."
            elif [[ $result -eq 124 ]] || [[ $result -eq 137 ]]; then
                lower='timeout'
                echo -n "x"
            else
                lower='error'
                echo -n "x"
            fi
            echo "$file $concurrency $info $lower $timing" >>$resultsfile
        done
    } < $infile
    echo
}

function compute_upper_lola {
    infile=$1
    resultsfile=$2
    {
        read header
        echo "$header upper_lola time_upper_lola" >$resultsfile
        while read file concurrency info; do
            lolafile=${file%.pnml}.lola

            mkdir -p ${outdir}/$(dirname $file)
            outfile=${outdir}/$file.upper_lola

            formula=${outdir}/$lolafile.upper.task
            k=$concurrency

            echo "REACHABLE (" >$formula
            sed -e '1,/PLACE/d' -e '/;/,$d' -e 's/,/ +/g' $lolafile >>$formula
            finalplaces=$(grep -A 1 "<finalmarking" $file | sed -e 's/\s*<text>\s*//g' -e 's/\s*<\/text>\s*//' -e '2p;d')
            for p in $finalplaces; do
                placename=$(grep -A 2 "id=\"$p\"" $file | sed -e 's/\s*<text>\s*//g' -e 's/\s*<\/text>\s*//' -e '3p;d')
                echo " - $placename" >>$formula
            done
            echo "> $k)" >>$formula

            timing="$(date +%s%N)"
            (
                set -o pipefail
                ulimit -S -v $mem_soft
                ulimit -H -v $mem_hard
                timeout $timelimit $lolaexe -f $formula $lolafile >$outfile 2>&1
            ) 2>/dev/null
            result=$?
            timing=$(($(date +%s%N)-timing))
            if [[ $result -eq 0 ]]; then
                if grep -q "lola: *result: *no" $outfile; then
                    upper=$k
                elif grep -q "lola: *result: *yes" $outfile; then
                    upper=">$k"
                else
                    upper='unknown'
                fi
                echo -n "."
            elif [[ $result -eq 124 ]] || [[ $result -eq 137 ]]; then
                upper='timeout'
                echo -n "x"
            else
                upper='error'
                echo -n "x"
            fi
            echo "$file $concurrency $info $upper $timing" >>$resultsfile
        done
    } < $infile
    echo
}

function determine_statespace_lola {
    infile=$1
    resultsfile=$2
    {
        read header
        echo "$header statespace">$resultsfile
        while read file info; do
            mkdir -p ${outdir}/$(dirname file)
            outfile=${outdir}/$file.statespace_lola
            lolafile=${file%.pnml}.lola

            #echo "Determining size of the statespace of $file"
            (
                set -o pipefail
                ulimit -S -v $mem_soft
                ulimit -H -v $mem_hard
                timeout $timelimit $lolaexe -c full $lolafile >$outfile 2>&1
            ) 2>/dev/null
            result=$?
            if [[ $result -eq 0 ]]; then
                statespace=$(sed -e '/^lola: [0-9]* markings/!D' -e 's/^lola: \([0-9]*\) markings.*$/\1/' $outfile)
                echo -n "."
            elif [[ $result -eq 124 ]] || [[ $result -eq 137 ]]; then
                statespace='timeout'
                echo -n "x"
            else
                statespace='error'
                echo -n "x"
            fi
            echo "$file $info $statespace" >>$resultsfile
        done
    } <$infile
    echo
}

function determine_statespace_tedd {
    infile=$1
    resultsfile=$2
    {
        read header
        echo "$header statespace">$resultsfile
        while read file info; do
            mkdir -p ${outdir}/$(dirname file)
            outfile=${outdir}/$file.statespace_tedd

            (
                set -o pipefail
                ulimit -S -v $mem_soft
                ulimit -H -v $mem_hard
                timeout $timelimit $teddexe -PNML -bdd $file >$outfile 2>&1
            ) 2>/dev/null
            result=$?
            if [[ $result -eq 0 ]]; then
                statespace=$(sed -e '/^[0-9.eE]* \(state\|marking\)/!D' -e 's/^\([0-9.eE]*\) \(state\|marking\).*$/\1/' $outfile)
                echo -n "."
            elif [[ $result -eq 124 ]] || [[ $result -eq 137 ]]; then
                statespace='timeout'
                echo -n "x"
            else
                statespace='error'
                echo -n "x"
            fi
            echo "$file $info $statespace" >>$resultsfile
        done
    } <$infile
    echo
}

function determine_statespace_dummy {
    infile=$1
    resultsfile=$2
    {
        read header
        echo "$header statespace">$resultsfile
        while read file info; do
            echo "$file $info 0" >>$resultsfile
        done
    } <$infile
    echo
}

function check_results {
    infile=$1
    {
        read header
        while read file concurrency places transitions acyclic markedgraph statespace upper_rat time_upper_rat upper_int time_upper_int lower_lola time_lower_lola upper_lola time_upper_lola; do
            if [[ "$upper_rat" =~ error|timeout ]]; then
                echo "For $file macaw could not compute an upper bound using rationals: $upper_rat"
            elif [[ "$upper_int" =~ error|timeout ]]; then
                echo "For $file macaw could not compute an upper bound using integers: $upper_int"
            elif [ "$upper_rat" != "$upper_int" ]; then
                echo "For $file upper bound by rationals and integers differ: $upper_rat $upper_int"
            fi

            if [[ "$lower_lola" =~ error|timeout ]]; then
                echo "For $file lola could not compute a lower bound: $lower_lola"
            elif [ "$lower_lola" != "$upper_int" ]; then
                echo "For $file upper bound by integers is not also a lower bound: $lower_lola $upper_int"
            elif [ "$lower_lola" != "$upper_rat" ]; then
                echo "For $file upper bound by rationals is not also a lower bound: $lower_lola $upper_rat"
            fi

            if [[ "$upper_lola" =~ error|timeout ]]; then
                echo "For $file lola could not compute an upper bound: $upper_lola"
            elif [ "$upper_lola" != "$upper_rat" ]; then
                echo "For $file upper bound by rationals is not an actual upper bound: $upper_lola $upper_rat"
            elif [ "$upper_lola" != "$upper_int" ]; then
                echo "For $file upper bound by integers is not an actual upper bound: $upper_lola $upper_int"
            fi
        done
    } <$infile
}

function make_histogram {
    infile=$1
    resultsfile=$2
    echo "concurrency nets" >$resultsfile
    {
        read header
        while read file concurrency places transitions acyclic markedgraph statespace upper_rat time_upper_rat upper_int time_upper_int lower_lola time_lower_lola upper_lola time_upper_lola; do
            echo $concurrency
        done
    } <$infile | sort -n | uniq -c | sed -e 's/^ *\([0-9][0-9]*\) *\([0-9][0-9]*\) *$/\2 \1/' >>$resultsfile
}

function filter_large {
    infile=$1
    resultsfile=$2
    {
        read header
        echo $header >$resultsfile
        while read file concurrency places transitions acyclic markedgraph statespace upper_rat time_upper_rat upper_int time_upper_int lower_lola time_lower_lola upper_lola time_upper_lola; do
            statespace_int=$(LC_NUMERIC="en_US.UTF-8" printf '%.0f\n' "$statespace")
            if [ "$statespace_int" -gt 1000000 ]; then
                statespace=$(LC_NUMERIC="en_US.UTF-8" printf "%g" "$statespace_int")
                time_upper_rat=$(echo "scale=2; $time_upper_rat / 1000000000" | bc -l)
                time_upper_int=$(echo "scale=2; $time_upper_int / 1000000000" | bc -l)
                time_lower_lola=$(echo "scale=2; $time_lower_lola / 1000000000" | bc -l)
                time_upper_lola=$(echo "scale=2; $time_upper_lola / 1000000000" | bc -l)
                echo $file $concurrency $places $transitions $acyclic $markedgraph $statespace $upper_rat $time_upper_rat $upper_int $time_upper_int $lower_lola $time_lower_lola $upper_lola $time_upper_lola >>$resultsfile
            fi
        done
    } <$infile
}

arg=$1
input=$2
output=$3

if [[ "$arg" == "soundness" ]]; then
    if ! [ -x "$(command -v $macawexe)" ]; then
        echo 'Error: macaw is not installed, cannot check soundness' >&2
        exit 1
    else
        check_soundness $input $output
    fi
elif [[ "$arg" == "statespace" ]]; then
    if ! [ -x "$(command -v $teddexe)" ]; then
        echo 'Error: tedd is not installed, cannot determine size of the state space' >&2
       determine_statespace_dummy $input $output
    else
        determine_statespace_tedd $input $output
    fi
elif [[ "$arg" == "lower" ]]; then
    if ! [ -x "$(command -v $macawexe)" ]; then
        echo 'Error: macaw is not installed, cannot compute lower bound' >&2
        exit 1
    else
        compute_lower_macaw $input $output
    fi
elif [[ "$arg" == "upper_rat" ]]; then
    if ! [ -x "$(command -v $macawexe)" ]; then
        echo 'Error: macaw is not installed, cannot compute upper bound' >&2
        exit 1
    else
        compute_upper_macaw $input $output rat
    fi
elif [[ "$arg" == "upper_int" ]]; then
    if ! [ -x "$(command -v $macawexe)" ]; then
        echo 'Error: macaw is not installed, cannot compute upper bound' >&2
        exit 1
    else
        compute_upper_macaw $input $output int
    fi
elif [[ "$arg" == "lower_lola" ]]; then
    if ! [ -x "$(command -v $lolaexe)" ]; then
        echo 'Error: lola is not installed, cannot compute lower bound' >&2
        exit 1
    else
        compute_lower_lola $input $output
    fi
elif [[ "$arg" == "upper_lola" ]]; then
    if ! [ -x "$(command -v $lolaexe)" ]; then
        echo 'Error: lola is not installed, cannot compute upper bound' >&2
        exit 1
    else
        compute_upper_lola $input $output
    fi
elif [[ "$arg" == "convert" ]]; then
    if ! [ -x "$(command -v $pntoolsdir/$pntoolsexe)" ]; then
        echo 'Error: pn_tools is not installed, cannot convert nets' >&2
        exit 1
    else
        convert_nets $input
    fi
elif [[ "$arg" == "adapt" ]]; then
    adapt_nets $input
elif [[ "$arg" == "check" ]]; then
    check_results $input
elif [[ "$arg" == "histogram" ]]; then
    make_histogram $input $output
elif [[ "$arg" == "filterlarge" ]]; then
    filter_large $input $output
else
    echo "Error: unknown command: $arg"
    exit 1
fi
