#! /usr/bin/env Rscript
args<-commandArgs(TRUE)

data <- read.table(args[1], sep=" ", header=T)

if ("time_upper_rat" %in% colnames(data)) {
    data$time_upper_rat <- (data$time_upper_rat / 1e9)
}
if ("time_upper_int" %in% colnames(data)) {
    data$time_upper_int <- (data$time_upper_int / 1e9)
}
if ("time_lower_lola" %in% colnames(data)) {
    data$time_lower_lola <- (data$time_lower_lola / 1e9)
}
if ("time_upper_lola" %in% colnames(data)) {
    data$time_upper_lola <- (data$time_upper_lola / 1e9)
}

#columns = c("places", "transitions", "concurrency", "upper_rat", "upper_int", "lower_lola", "upper_lola")
#sel <- subset(data, select=columns)

print("Number of sound free-choice workflow nets:")
print(nrow(data))

if ("markedgraph" %in% colnames(data)) {
    mg <- subset(data, (data$markedgraph == 'yes'))
    print("Number of sound marked graphs:")
    print(nrow(mg))
    if ("acyclic" %in% colnames(data)) {
        nmg_ac <- subset(data, (data$markedgraph == 'no' & data$acyclic == 'yes'))
        nmg_c <- subset(data, (data$markedgraph == 'no' & data$acyclic == 'no'))
        print("Number of sound ayclic nets that are not marked graphs:")
        print(nrow(nmg_ac))
        print("Number of sound cyclic nets that are not marked graphs:")
        print(nrow(nmg_c))
    }
}

print("Statistics for number of places:")
print(summary(subset(data, select=c("places"))))
print("Statistics for number of transitions:")
print(summary(subset(data, select=c("transitions"))))
print("Statistics for concurrency threshold:")
print(summary(subset(data, select=c("concurrency"))))

if ("statespace" %in% colnames(data)) {
    print("Statistics for size of the state space:")
    print(summary(subset(data, select=c("statespace"))))
}

if ("upper_rat" %in% colnames(data)) {
    print("Statistics for time on upper bound by linear program using rationals:")
    print(summary(subset(data, select=c("time_upper_rat"))))
    print("Sum of time for upper bound by linear program using rationals:")
    print(sum(data$time_upper_rat))
}

if ("upper_int" %in% colnames(data)) {
    print("Statistics for time on upper bound by linear program using integers:")
    print(summary(subset(data, select=c("time_upper_int"))))
    print("Sum of time for upper bound by linear program using integers:")
    print(sum(data$time_upper_int))
}

if ("lower_lola" %in% colnames(data)) {
    print("Statistics for time on lower bound by lola:")
    print(summary(subset(data, select=c("time_lower_lola"))))
    print("Sum of time for lower bound by lola:")
    print(sum(data$time_lower_lola))
}

if ("upper_lola" %in% colnames(data)) {
    error <- subset(data, (data$upper_lola %in% c('error', 'timeout')))
    noerror <- subset(data, !(data$upper_lola %in% c('error', 'timeout')))

    print("Number of errors or timeouts for upper bound by lola:")
    print(nrow(error))
    print("Statistics for time on upper bound by lola (excluding errors and timeouts):")
    print(summary(subset(noerror, select=c("time_upper_lola"))))
    print("Sum of time for upper bound by lola (excluding errors and timeouts):")
    print(sum(noerror$time_upper_lola))
}
