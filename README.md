Macaw
=======

Compute bounds on the concurrency threshold of a workflow net.

Installation
-------

To build Macaw, the following dependencies need to be installed:

- The [Rust](https://www.rust-lang.org/) compiler and standard library.
- [Cargo](https://crates.io/) as a build tool and to install rust dependencies.
- The [GSL library](http://www.gnu.org/software/gsl/) for linear algebra routines.
- The [COIN-OR Cbc](https://projects.coin-or.org/Cbc) tool to solve linear programs.

To then compile Macaw, go to the project's main directory and enter:
```bash
    make build
```

To install Macaw, issue the following command:
```bash
    make install
```
By default, the binary is installed in `~/.cargo/bin`.

Input
------

Macaw takes as input a single workflow net described in [PNML](http://www.pnml.org/).
Example nets are included in the `examples` directory.

Usage
-------

Macaw is run from the command line. Assuming the macaw binary is in the system path,
you can compute an upper bound on the concurrency threshold of one of the example nets
by the following command:
```bash
    macaw -u examples/tacas_example_net.pnml
```
You then obtain the following output:
```
Upper bound on concurrency threshold: 3
```

Some possible options for computing the bound on the concurrency threshold
and analyzing the net are:

- `-u`, `--upper`: Computes an upper bound on the concurrency threshold using a linear program.
- `-n`, `--natural`: Use natural (integer) variables for the linear program.
- `-i`, `--info`: Print information on the size and type of the net.
- `-m`, `--markoutputs`: Apply a transformation to mark all output places in the final marking.
- `-d`, `--disregardoutputs`: Do not consider output places for the concurrency threshold.
- `-o <lpfile>`: Write constructed linear program to this file
- `-s`, `--soundness`: Check if the input net is a sound free-choice workflow net.

To see all options offered by Macaw, enter:
```bash
    macaw --help
```

Benchmarks
-------

In the `benchmarks` directory, workflow nets from a benchmark suite and scripts to
evaluate the concurrency threshold of these nets can be found. To compile a list
of results for all sound workflow nets in the benchmark suite, go to the
`benchmarks` directory and run:
```bash
    make
```
The results will be compiled into the file `results.csv` and statistics on the results
will be displayed.

In addition to the above requirements to build Macaw, the following dependencies need
to be met if the corresponding values should be computed:

- The tool [LoLA](http://service-technology.org/lola/) needs to be installed
and its executable contained in the system path to compute lower and upper bounds using LoLA.
- For determining the size of the statespace, the tool
tedd from the [TINA](http://projects.laas.fr/tina/) toolbox is required.
Unfortunately, tedd is not publicly available, but it is not a prerequisite
to recreate the benchmark results for the concurrency threshold.
- For computing statistics on the results, the [R Project](https://www.r-project.org/)
software environment needs to be installed.

The nets used for the benchmarks were originally downloaded
from the [BIT Process Library](https://user.enterpriselab.ch/~takoehle/) and were provided by IBM.
These nets are given in the PNML and LoLA format.

Authors
-------

* Philipp Meyer (<meyerphi@in.tum.de>)

License
-------
Macaw is licensed under the GNU GPLv3, see [LICENSE.md](LICENSE.md) for details.

Logo by [Thomas Voekler](https://commons.wikimedia.org/wiki/User:Teacoolish), [Anodorhynchus hyacinthinus](https://commons.wikimedia.org/wiki/File:Anodorhynchus_hyacinthinus.jpg), [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/legalcode).
