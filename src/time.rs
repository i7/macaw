/**
 * Macaw - Compute bounds on the concurrency threshold of a workflow net
 *
 *  Copyright 2018 by Philipp Meyer <meyerphi@in.tum.de>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See LICENSE.md and README.md.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

use std::collections::HashMap;
use std::collections::BTreeMap;
use std::collections::VecDeque;
use std::cmp;
use num_rational::Rational64;
use num_traits::identities::{One, Zero};

use net::{PetriNet, Transition, Weight, Time};
use cli;

pub type Probability = Weight;
pub type Marking = Vec<u32>;
pub type TimedMarking = Vec<Time>;
pub type Node = usize;
pub type MCTransition = (Transition, Probability, Node);
pub type Distribution = HashMap<TimedMarking, Probability>;
pub type TimeDistribution = BTreeMap<Time, Probability>;

pub struct MarkovChain {
    pub markings: Vec<Marking>,
    pub transitions: Vec<Vec<MCTransition>>,
    pub predecessors: Vec<Vec<MCTransition>>,
}

pub fn compute_expected_time(net: &PetriNet) -> Result<Rational64, cli::CliError> {
    let chain = compute_markov_chain(net);
    let dist = compute_distribution(net, &chain);
    println!("Time distribution:");
    let mut expected = Rational64::zero();
    for (time, prob) in dist {
        println!("{}: {}", time, prob);
        expected += prob * time;
    }
    Ok(expected)
}

fn print_marking(net: &PetriNet, marking: &Marking) {
    print!("[");
    for p in 0..net.num_places() {
        if marking[p] > 0 {
            print!(" {}", net.places[p]);
        }
    }
    println!(" ]");
}

fn compute_successor(net: &PetriNet, marking: &Marking, t: Transition) -> Marking {
    let mut new_marking = marking.clone();
    for &p in net.pre_t(t) {
        new_marking[p] -= 1;
    }
    for &p in net.post_t(t) {
        new_marking[p] += 1;
    }
    new_marking
}

fn compute_distribution(net: &PetriNet, chain: &MarkovChain) -> TimeDistribution {
    let mut dists = vec![];
    let mut final_node = 0;
    let i = net.p_input[0];
    let o = net.p_output[0];

    for n in 0..chain.markings.len() {
        let tm = vec![Time::zero(); net.num_places()];
        let mut dist: Distribution = HashMap::new();
        dist.insert(tm, Probability::one());
        dists.push(dist);
        if chain.markings[n][o] == 1 {
            final_node = n
        }
    }

    // TODO: find number of iterations or check convergence with epsilon
    for iter in 0..100 {
        for n in 0..chain.markings.len() {
            if chain.markings[n][i] != 1 {
                let mut dist_new = HashMap::new();
                for &(t, prob_t, np) in &chain.predecessors[n] {
                    let time_t = net.t_time[t];
                    for (tm, prob) in &dists[np] {
                        let mut tm_new = tm.clone();
                        let mut pre_time = Time::zero();
                        for &p in net.pre_t(t) {
                            pre_time = pre_time.max(tm[p]);
                            tm_new[p] = Time::zero();
                        }
                        for &p in net.post_t(t) {
                            tm_new[p] = pre_time + time_t;
                        }
                        let prob_new = prob * prob_t.clone();
                        let prob_old = dist_new.get(&tm_new).cloned().unwrap_or(Probability::zero());
                        dist_new.insert(tm_new, prob_old + prob_new);
                    }
                }
                dists[n] = dist_new;
            }
        }
    }

    let mut tdist = BTreeMap::new();
    let final_dist = &dists[final_node];
    for (tm, &prob) in final_dist {
        tdist.insert(tm[o] as Time, prob);
    }
    tdist
}

fn compute_markov_chain(net: &PetriNet) -> MarkovChain {
    let mut chain = MarkovChain {
        markings: vec![], transitions: vec![], predecessors: vec![]
    };

    let mut map: HashMap<Marking, Node> = HashMap::new();
    let mut queue = VecDeque::new();
    {
        let mut mi = vec![0; net.num_places()];
        mi[net.p_input[0]] = 1;
        queue.push_back(0);
        map.insert(mi.clone(), 0);
        chain.markings.push(mi);
        chain.transitions.push(vec![]);
        chain.predecessors.push(vec![]);
    }
    while let Some(n) = queue.pop_front() {
        print!("* Adding marking {} = ", n);
        print_marking(&net, &chain.markings[n]);
        for c in 0..net.clusters.len() {
            let mut enabled = true;
            for &p in &net.clusters[c] {
                if chain.markings[n][p] == 0 {
                    enabled = false;
                    break;
                }
            }
            if enabled {
                print!("  Cluster {} = [", c);
                for &p in &net.clusters[c] {
                    print!(" {}", net.places[p]);
                }
                let post = net.post_p(net.clusters[c][0]);
                let mut denom = Weight::zero();
                print!(" ] enables [");
                for &t in post {
                    print!(" {}", net.transitions[t]);
                    denom += net.t_weight[t];
                }
                println!(" ]");

                for &t in post {
                    let mp = compute_successor(&net, &chain.markings[n], t);
                    let np =
                        if let Some(&np) = map.get(&mp) { np }
                        else {
                            let np = chain.markings.len();
                            map.insert(mp.clone(), np);
                            chain.markings.push(mp);
                            chain.transitions.push(vec![]);
                            chain.predecessors.push(vec![]);
                            queue.push_back(np);
                            np
                        };
                    print!("  Transition {} leads to {} = ", net.transitions[t], np);
                    print_marking(&net, &chain.markings[np]);
                    let prob = net.t_weight[t] / denom;
                    chain.transitions[n].push((t, prob, np));
                    chain.predecessors[np].push((t, prob, n));
                }
                break;
            }
        }
    }
    chain
}
