/**
 * Macaw - Compute bounds on the concurrency threshold of a workflow net
 *
 *  Copyright 2018 by Philipp Meyer <meyerphi@in.tum.de>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See LICENSE.md and README.md.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_xml_rs;
#[macro_use]
extern crate clap;

extern crate num_traits;
extern crate num_rational;

#[cfg(feature = "color")]
extern crate ansi_term;
#[cfg(feature = "color")]
extern crate atty;

extern crate lp_modeler;
extern crate rgsl;

#[macro_use]
extern crate log;
extern crate simple_logger;

#[macro_use]
mod fmt;

mod pnml;
mod net;
mod cli;
mod mc;
mod time;

use log::{LogLevel};

use std::io::BufReader;
use std::fs::File;

use net::{PetriNet};

fn read_net(path: &String, markoutputs: bool, unweighted: bool) -> Result<PetriNet, cli::CliError> {
    let file = File::open(path)?;
    let reader = BufReader::new(file);
    let pnml: pnml::PNML = serde_xml_rs::deserialize(reader)?;
    PetriNet::try_from(pnml, markoutputs, unweighted)
}

fn real_main() -> Result<(), cli::CliError> {
    let matches = clap_app!(macaw =>
        (version: "0.2.0")
        (author: "Philipp Meyer <meyerphi@in.tum.de>")
        (about: "Compute bounds on the concurrency threshold of a workflow net")
        (@arg INPUT: +required "Sets the input file to use")
        (@group analysis =>
            (@attributes +required ...)
            (@arg info: -i --info "Print information on size of the net")
            (@arg soundness: -s --soundness "Check if net is a sound free-choice workflow net")
            (@arg lower: -l --lower "Compute lower bound using underapproximation")
            (@arg upper: -u --upper "Compute upper bound using linear program")
            (@arg time: -t --time "Compute expected time of timed probabilistic workflow net")
        )
        (@arg markoutputs: -m --markoutputs "Apply transform to mark all output places in the final marking")
        (@arg natural: -n --natural "Use natural numbers to obtain upper bound")
        (@arg lpfile: -o --output +takes_value "Write constructed linear program to this file")
        (@arg disregardoutputs: -d --disregardoutputs "Disregard output places for the concurrency threshold")
        (@arg verbose: -v --verbose ... "Increase verbosity")
    ).get_matches();
    // Get options
    let path = matches.value_of("INPUT").unwrap().to_string();
    let log_level = match matches.occurrences_of("verbose") {
        0 => LogLevel::Warn,
        1 => LogLevel::Info,
        2 => LogLevel::Debug,
        3 | _ => LogLevel::Trace
    };
    simple_logger::init_with_level(log_level)?;
    let markoutputs = matches.is_present("markoutputs");
    let info = matches.is_present("info");
    let natural = matches.is_present("natural");
    let disregardoutputs = matches.is_present("disregardoutputs");
    let soundness = matches.is_present("soundness");
    let lower = matches.is_present("lower");
    let upper = matches.is_present("upper");
    let time = matches.is_present("time");
    let lpfile = matches.value_of("lpfile");

    // Read and analyze net
    log!(LogLevel::Info, "Parsing input file");
    let mut net = read_net(&path, markoutputs, disregardoutputs)?;
    if info {
        net.print_information();
    }
    log!(LogLevel::Debug, "Petri net:\n{}", net);
    log!(LogLevel::Info, "Analyzing net");
    if soundness {
        net.analyze(markoutputs)?;
    }
    if lower {
        let (lower_bound, reductions, inexact_reductions) = mc::reduce_net(&mut net)?;
        if info {
            println!("Number of reductions: {}", reductions);
            println!("Number of inexact reductions: {}", inexact_reductions);
        }
        println!("Lower bound on concurrency threshold: {}", lower_bound);
    }
    if upper {
        let upper_bound = net.get_concurrency_upper_bound(lpfile, natural)?;
        println!("Upper bound on concurrency threshold: {}", upper_bound);
    }
    if time {
        let expected_time = time::compute_expected_time(&net)?;
        println!("Expected time: {}", expected_time);
    }

    Ok(())
}

fn main() {
    match real_main() {
        Result::Ok(_) => (),
        Result::Err(err) => err.exit()
    }
}
