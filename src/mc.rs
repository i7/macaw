/**
 * Macaw - Compute bounds on the concurrency threshold of a workflow net
 *
 *  Copyright 2018 by Philipp Meyer <meyerphi@in.tum.de>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See LICENSE.md and README.md.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

use log::{LogLevel};
use std;

use net::{PetriNet, PInvariant, Place, Transition, Cluster, Count};
use cli;

pub fn reduce_net(net: &mut PetriNet) -> Result<(Count, Count, Count), cli::CliError> {
    match check_net_reduced(net)? {
        Some(mc) => return Ok((mc, 0, 0)),
        None => ()
    }

    let mut p_invariant_cluster = net.find_positive_p_invariant_for_clusters()?;
    let mut c_order: Vec<Cluster> = (0..net.num_clusters()).collect();
    c_order.sort_by(|&a, &b| p_invariant_cluster[a].partial_cmp(&p_invariant_cluster[b]).unwrap());

    let mut c_groups: Vec<Vec<Cluster>> = vec![];
    let mut cur_val = std::f32::MAX;
    let mut cur_group = vec![];
    for &c in &c_order {
        let val = p_invariant_cluster[c];
        if val > cur_val {
            c_groups.push(cur_group);
            cur_group = vec![];
        }
        cur_group.push(c);
        cur_val = val;
    }
    c_groups.push(cur_group);

    reduce_groups(net, &mut p_invariant_cluster, c_groups)
}

pub fn check_net_reduced(net: &mut PetriNet) -> Result<Option<Count>, cli::CliError> {
    if net.is_marked_graph() {
        let mc = net.get_concurrency_of_marked_graph()?;
        Ok(Some(mc))
    }
    else if net.is_state_machine() {
        let mc = net.get_concurrency_of_state_machine();
        Ok(Some(mc))
    }
    else {
        Ok(None)
    }
}

pub fn reduce_groups(
        net: &mut PetriNet, p_invariant_cluster: &mut PInvariant,
        c_groups: Vec<Vec<Cluster>>) -> Result<(Count, Count, Count), cli::CliError>
{
    let final_cluster = net.p_cluster[net.p_output[0]];
    let mut reductions = 0;
    let mut inexact_reductions = 0;

    for group in c_groups {
        for &c in &group {
            if c != final_cluster {
                let cpost = net.post_p(net.clusters[c][0]).clone();
                for t in cpost {
                    let (subnet, outputs) = reduce_cluster_transition(net, p_invariant_cluster, c, t);
                    if !subnet.is_state_machine() {
                        log!(LogLevel::Info, "Reducing cluster {:} with transition {}", net.get_cluster(c), net.transitions[t]);
                        log!(LogLevel::Debug, "Subnet:\n{}", subnet);
                        let output_cluster = net.p_cluster[outputs[0]];
                        for &p in &outputs {
                            if output_cluster != net.p_cluster[p] {
                                inexact_reductions += 1;
                                break;
                            }
                        }
                        replace_subnet(net, p_invariant_cluster, c, Some(t), subnet, outputs)?;
                        log!(LogLevel::Debug, "Reduced net:\n{}", net);
                        reductions += 1;
                    }
                    match check_net_reduced(net)? {
                        Some(mc) => return Ok((mc, reductions, inexact_reductions)),
                        None => ()
                    }
                }
            }
        }
        for &c in &group {
            if c != final_cluster {
                let (subnet, outputs) = reduce_cluster(net, p_invariant_cluster, c);
                if !subnet.is_marked_graph() {
                    log!(LogLevel::Info, "Reducing cluster {:}", net.get_cluster(c));
                    log!(LogLevel::Debug, "Subnet:\n{}", subnet);
                    replace_subnet(net, p_invariant_cluster, c, None, subnet, outputs)?;
                    log!(LogLevel::Debug, "Reduced net:\n{}", net);
                    reductions += 1;
                }
                match check_net_reduced(net)? {
                    Some(mc) => return Ok((mc, reductions, inexact_reductions)),
                    None => ()
                }
            }
        }
    }

    unreachable!("Net should be reduced")
}

pub fn replace_subnet(
        net: &mut PetriNet, p_invariant_cluster: &mut PInvariant,
        cluster: Cluster, transition: Option<Transition>,
        subnet: PetriNet, outputs: Vec<Place>) -> Result<Count, cli::CliError>
{
    let val_subnet = p_invariant_cluster[cluster];
    /* TODO sanity check
    let subnet_p_invariant_cluster = subnet.check_soundness().unwrap();
    let subnet_ts = subnet.reset_transition;
    let mc_subnet = subnet_p_invariant_cluster[subnet.t_cluster[subnet_ts]] as Count;
    */

    let mc_subnet = {
        if transition.is_none() || subnet.num_places() <= 3 || subnet.num_transitions() <= 1 {
            subnet.get_concurrency_of_state_machine()
        }
        else {
            subnet.get_concurrency_of_marked_graph()?
        }
    };

    // add new place and transitions
    let place_name = if let Some(t) = transition {
        format!("p_t{}", t)
    }
    else {
        format!("p_c{}", cluster)
    };
    let new_p = net.add_place(place_name, mc_subnet);
    let t_initial = if let Some(t) = transition { t } else {
        net.add_transition(format!("_c{}_ti_", cluster))
    };
    let t_final = net.add_transition(format!("_c{}_to_", cluster));

    // actually replace subnet
    if let Some(t) = transition {
        let tpost = net.post_t(t).clone();
        for p in tpost {
            net.context_p[p].0.retain(|&t2| t2 != t);
        }
        net.context_t[t].1.clear();
    }
    else {
        let c_places = net.clusters[cluster].clone();
        let cpost = net.post_p(c_places[0]).clone();
        for &p in &c_places {
            net.context_p[p].1.clear();
        }
        for t in cpost {
            net.context_t[t].0.clear();
        }
        for &p in &c_places {
            net.add_p_t_arc(p, t_initial);
        }
    }
    net.add_t_p_arc(t_initial, new_p);
    net.add_p_t_arc(new_p, t_final);

    for p in outputs {
        net.add_t_p_arc(t_final, p);
    }
    net.add_clusters();
    p_invariant_cluster.push(val_subnet);

    net.sort_pre_postsets();
    net.remove_unreachable();

    Ok(mc_subnet)
}

fn reduce_cluster_transition(net: &PetriNet, p_invariant_cluster: &PInvariant, cluster: Cluster, transition: Transition) ->
        (PetriNet, Vec<Place>) {
    reduce(net, p_invariant_cluster, cluster, Some(transition), |v1, v2| v1 > v2)
}
fn reduce_cluster(net: &PetriNet, p_invariant_cluster: &PInvariant, cluster: Cluster) ->
        (PetriNet, Vec<Place>) {
    reduce(net, p_invariant_cluster, cluster, None, |v1, v2| v1 >= v2)
}

fn reduce<F>(net: &PetriNet, p_invariant_cluster: &PInvariant, cluster: Cluster, transition: Option<Transition>, valid_successor: F) ->
    (PetriNet, Vec<Place>) where F: Fn(f32, f32) -> bool
{
    let val = p_invariant_cluster[cluster];

    let mut subnet_places = vec![false; net.num_places()];
    let mut subnet_transitions = vec![false; net.num_transitions()];
    let ts = net.reset_transition;

    let mut t_count = vec![0; net.num_transitions()];
    for t in 0..net.num_transitions() {
        t_count[t] = net.pre_t(t).len();
    }
    let mut p_count = vec![0; net.num_places()];

    let mut stack = vec![];

    let inputs = match transition {
        Some(t) => net.post_t(t),
        None => &net.clusters[cluster],
    };

    if let Some(t) = transition {
        stack.push(t);
    }
    else {
        let post = net.post_p(net.clusters[cluster][0]);
        for &t in post {
            subnet_transitions[t] = true;
            stack.push(t);
        }
        for &p in &net.clusters[cluster] {
            p_count[p] = post.len();
            subnet_places[p] = true;
        }
    }

    while let Some(t) = stack.pop() {
        for &p in net.post_t(t) {
            if !subnet_places[p] {
                subnet_places[p] = true;
                let val_p = p_invariant_cluster[net.p_cluster[p]];
                if valid_successor(val, val_p) {
                    for &t2 in net.post_p(p) {
                        t_count[t2] -= 1;
                        if t2 != ts && t_count[t2] == 0 {
                            subnet_transitions[t2] = true;
                            stack.push(t2);
                            for &p2 in net.pre_t(t2) {
                                p_count[p2] += 1;
                            }
                        }
                    }
                }
            }
        }
    }

    let mut outputs = vec![];
    for p in 0..net.num_places() {
        if subnet_places[p] && p_count[p] == 0 {
            outputs.push(p);
        }
    }
    outputs.sort();

    let subnet = net.construct_subnet(&subnet_places, &subnet_transitions, &inputs, &outputs);

    (subnet, outputs)
}
