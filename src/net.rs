/**
 * Macaw - Compute bounds on the concurrency threshold of a workflow net
 *
 *  Copyright 2018 by Philipp Meyer <meyerphi@in.tum.de>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See LICENSE.md and README.md.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

use std::collections::HashMap;
use std::fmt as std_fmt;
use std::fmt::Display;
use std;
use std::cmp;
use std::str::FromStr;

use log::{LogLevel};

use lp_modeler::problem::{LpObjective, LpProblem, LpFileFormat};
use lp_modeler::operations::{LpOperations};
use lp_modeler::variables::{LpContinuous, LpInteger, BoundableLp, LpExpression};
use lp_modeler::solvers::{SolverTrait, CbcSolver, Status};

use rgsl::Value;
use rgsl::types::{MatrixF64, VectorF64};
use rgsl::linear_algebra::SV_decomp;

use num_rational::Rational64;
use num_traits::identities::{One, Zero};

use pnml;
use cli;
use fmt;

pub type Place = usize;
pub type Transition = usize;
pub type Cluster = usize;
pub type Count = u32;
pub type Weight = Rational64;
pub type Time = i64;

const NO_CLUSTER: Cluster = std::usize::MAX;

#[derive(Debug)]
pub struct PetriNet {
    pub places: Vec<String>,
    pub transitions: Vec<String>,
    pub context_p: Vec<(Vec<Transition>, Vec<Transition>)>,
    pub context_t: Vec<(Vec<Place>, Vec<Place>)>,
    pub p_weight: Vec<Count>,
    pub p_input: Vec<Place>,
    pub p_output: Vec<Place>,
    pub p_cluster: Vec<Cluster>,
    pub t_cluster: Vec<Cluster>,
    pub clusters: Vec<Vec<Place>>,
    pub reset_transition: Transition,
    pub t_weight: Vec<Weight>,
    pub t_time: Vec<Time>,
}

#[derive(Debug)]
pub struct ClusterObj<'a> {
    net: &'a PetriNet,
    c: Cluster
}

impl<'a> Display for ClusterObj<'a> {
    fn fmt(&self, f: &mut std_fmt::Formatter) -> std_fmt::Result {
        write!(f, "{{ ")?;
        for &p in &self.net.clusters[self.c] {
            write!(f, "{} ", &self.net.places[p])?;
        }
        write!(f, "}}")?;

        Ok(())
    }
}

pub type PInvariant = Vec<f32>;
pub type TInvariant = Vec<f32>;

impl PetriNet {
    pub fn new() -> PetriNet {
        PetriNet {
            places: vec![],
            transitions: vec![],
            context_p: vec![],
            context_t: vec![],
            p_weight: vec![],
            p_input: vec![],
            p_output: vec![],
            p_cluster: vec![],
            t_cluster: vec![],
            clusters: vec![],
            reset_transition: 0,
            t_weight: vec![],
            t_time: vec![],
        }
    }

    pub fn add_place(&mut self, p: String, w: Count) -> Place {
        let i = self.num_places();
        self.places.push(p);
        self.p_weight.push(w);
        self.context_p.push((vec![], vec![]));
        self.p_cluster.push(NO_CLUSTER);
        i
    }

    pub fn add_transition(&mut self, t: String) -> Transition {
        let i = self.num_transitions();
        self.transitions.push(t);
        self.context_t.push((vec![], vec![]));
        self.t_cluster.push(NO_CLUSTER);
        self.t_weight.push(Weight::one());
        self.t_time.push(Time::zero());
        i
    }

    pub fn construct_subnet(&self,
                            new_places: &Vec<bool>,
                            new_transitions: &Vec<bool>,
                            inputs: &Vec<Place>,
                            outputs: &Vec<Place>) -> PetriNet {
        let mut subnet = PetriNet::new();
        let mut place_map = vec![0; self.num_places()];
        for p in 0..self.num_places() {
            if new_places[p] {
                place_map[p] = subnet.add_place(self.places[p].clone(), self.p_weight[p]);
            }
        }
        for t in 0..self.num_transitions() {
            if new_transitions[t] {
                let t_new = subnet.add_transition(self.transitions[t].clone());
                for &p in self.pre_t(t) {
                    if new_places[p] {
                        let p_new = place_map[p];
                        subnet.add_p_t_arc(p_new, t_new);
                    }
                }
                for &p in self.post_t(t) {
                    if new_places[p] {
                        let p_new = place_map[p];
                        subnet.add_t_p_arc(t_new, p_new);
                    }
                }
            }
        }
        // custom init

        // add input output places
        for &p in inputs {
            subnet.p_input.push(place_map[p]);
        }
        for &p in outputs {
            subnet.p_output.push(place_map[p]);
        }

        subnet.add_reset_transition();
        subnet.sort_pre_postsets();
        subnet.add_clusters();
        subnet
    }

    pub fn remove_unreachable(&mut self) {
        let mut p_reach = vec![false; self.places.len()];
        let mut t_reach = vec![false; self.transitions.len()];
        for &i in &self.p_input {
            let mut stack = vec![i];
            p_reach[i] = true;
            while let Some(p) = stack.pop() {
                for &t in self.post_p(p) {
                    if !t_reach[t] {
                        t_reach[t] = true;
                        for &p in self.post_t(t) {
                            if !p_reach[p] {
                                p_reach[p] = true;
                                stack.push(p);
                            }
                        }
                    }
                }
            }
        }
        for p in 0..self.num_places() {
            if !p_reach[p] {
                let ppre = self.pre_p(p).clone();
                let ppost = self.post_p(p).clone();
                for t in ppre {
                    self.context_t[t].1.retain(|&p2| p2 != p);
                }
                for t in ppost {
                    self.context_t[t].0.retain(|&p2| p2 != p);
                }
                self.context_p[p].0.clear();
                self.context_p[p].1.clear();
            }
        }
        for t in 0..self.num_transitions() {
            if !t_reach[t] {
                let tpre = self.pre_t(t).clone();
                let tpost = self.post_t(t).clone();
                for p in tpre {
                    self.context_p[p].1.retain(|&t2| t2 != t);
                }
                for p in tpost {
                    self.context_p[p].0.retain(|&t2| t2 != t);
                }
                self.context_t[t].0.clear();
                self.context_t[t].1.clear();
            }
        }
    }

    pub fn add_p_t_arc(&mut self, p: Place, t: Transition) {
        self.context_p[p].1.push(t);
        self.context_t[t].0.push(p);
    }

    pub fn add_t_p_arc(&mut self, t: Transition, p: Place) {
        self.context_p[p].0.push(t);
        self.context_t[t].1.push(p);
    }

    pub fn with_arcs(
        places: Vec<String>,
        transitions: Vec<(String, Vec<pnml::ToolSpecific>)>,
        arcs: Vec<(String, String)>,
        multioutput: bool, unweighted: bool
    ) -> Result<PetriNet, cli::CliError> {
        let c = fmt::Colorizer::new(fmt::ERR_COLORIZER);

        let n = places.len();
        let m = transitions.len();

        let mut net = PetriNet {
            places: places,
            transitions: vec![],
            context_p: vec![(vec![], vec![]); n],
            context_t: vec![(vec![], vec![]); m],
            p_weight: vec![0; n],
            p_input: vec![],
            p_output: vec![],
            p_cluster: vec![NO_CLUSTER; n],
            t_cluster: vec![NO_CLUSTER; m],
            clusters: vec![],
            reset_transition: 0,
            t_weight: vec![Weight::one(); m],
            t_time: vec![Time::zero(); m],
        };

        for (i, (t, toolspecific)) in transitions.into_iter().enumerate() {
            net.transitions.push(t);
            for tool in toolspecific {
                if tool.tool == "StochasticPetriNet" {
                    if let Some(w) = tool.properties.get("weight") {
                        net.t_weight[i] = Weight::from_str(w)?;
                    }
                    if let Some(dist) = tool.properties.get("distributionType") {
                        if dist == "DETERMINISTIC" {
                            if let Some(t) = tool.properties.get("distributionParameters") {
                                net.t_time[i] = Time::from_str(t)?;
                            }
                        }
                    }
                }
            }
        }

        {
            // lookup tables for indices of places and transitions by name
            let mut map_p: HashMap<&str, Place> = HashMap::new();
            let mut map_t: HashMap<&str, Transition> = HashMap::new();
            for (i, p) in net.places.iter().enumerate() {
                map_p.insert(p, i);
            }
            for (i, t) in net.transitions.iter().enumerate() {
                map_t.insert(t, i);
            }

            // construct pre- and postset of places and transitions
            for (x, y) in arcs {
                if let Some(&p) = map_p.get(&x[..]) {
                    if let Some(&t) = map_t.get(&y[..]) {
                        net.context_t[t].0.push(p);
                        net.context_p[p].1.push(t);
                    }
                    else if let Some(_) = map_p.get(&y[..]) {
                        let message = format!("Two places in arc ('{}', '{}')", x, y);
                        return Err(cli::CliError::with_message(message))
                    }
                    else {
                        let message = format!("Unknown place or transition '{}' in arc ('{}', '{}')", c.warning(&y), x, y);
                        return Err(cli::CliError::with_message(message))
                    }
                }
                else if let Some(&t) = map_t.get(&x[..]) {
                    if let Some(&p) = map_p.get(&y[..]) {
                        net.context_p[p].0.push(t);
                        net.context_t[t].1.push(p);
                    }
                    else if let Some(_) = map_t.get(&y[..]) {
                        let message = format!("Two transitions in arc ('{}', '{}')", c.none(x), c.none(y));
                        return Err(cli::CliError::with_message(message))
                    }
                    else {
                        let message = format!("Unknown place or transition '{}' in arc ('{}', '{}')", c.warning(&y), x, y);
                        return Err(cli::CliError::with_message(message))
                    }
                }
                else {
                    let message = format!("Unknown place or transition '{}' in arc ('{}', '{}')", c.warning(&x), x, y);
                    return Err(cli::CliError::with_message(message))
                }
            }
        }

        net.init(multioutput, unweighted);

        Ok(net)
    }

    fn init(&mut self, multioutput: bool, unweighted: bool) {
        self.add_input_output_places(multioutput);
        self.add_weights(unweighted);
        self.add_reset_transition();
        self.sort_pre_postsets();
        self.add_clusters();
    }

    pub fn sort_pre_postsets(&mut self) {
        for &mut (ref mut pre, ref mut post) in &mut self.context_p {
            pre.sort();
            post.sort();
        }
        for &mut (ref mut pre, ref mut post) in &mut self.context_t {
            pre.sort();
            post.sort();
        }
    }

    fn add_weights(&mut self, unweighted: bool) {
        for p in 0..self.num_places() {
            self.p_weight[p] = 1;
        }
        if unweighted {
            for &i in &self.p_input {
                self.p_weight[i] = 0;
            }
            for &o in &self.p_output {
                self.p_weight[o] = 0;
            }
        }
    }

    fn add_input_output_places(&mut self, multioutput: bool) {
        for (p, &(ref pre, ref post)) in self.context_p.iter().enumerate() {
            if pre.is_empty() {
                self.p_input.push(p)
            }
            if post.is_empty() {
                self.p_output.push(p)
            }
        }

        if multioutput {
            let mut p_reach = vec![NO_CLUSTER; self.places.len()];
            let mut t_reach = vec![NO_CLUSTER; self.transitions.len()];

            let output = self.p_output.clone();
            for o in output {
                let mut stack = vec![o];
                p_reach[o] = o;
                while let Some(p) = stack.pop() {
                    for &t in self.pre_p(p) {
                        if t_reach[t] != o {
                            t_reach[t] = o;
                            for &p in self.pre_t(t) {
                                if p_reach[p] != o {
                                    p_reach[p] = o;
                                    stack.push(p);
                                }
                            }
                        }
                    }
                }
                for t in 0..self.num_transitions() {
                    let mut pre_inter = false;
                    let mut post_inter = false;
                    for &p in self.pre_t(t) {
                        if p_reach[p] == o {
                            pre_inter = true;
                            break;
                        }
                    }
                    for &p in self.post_t(t) {
                        if p_reach[p] == o {
                            post_inter = true;
                            break;
                        }
                    }
                    if self.post_t(t).len() >= 1 && pre_inter && !post_inter {
                        self.add_t_p_arc(t, o);
                    }
                }
            }
        }
    }

    fn add_reset_transition(&mut self) {
        let ts = self.add_transition("_ts_".to_string());
        self.reset_transition = ts;

        let inputs = &self.p_input.clone();
        for &p in inputs {
            self.add_t_p_arc(ts, p);
        }
        let outputs = &self.p_output.clone();
        for &p in outputs {
            self.add_p_t_arc(p, ts);
        }
    }

    pub fn add_clusters(&mut self) {
        let mut c = self.clusters.len();
        let mut p_cluster = self.p_cluster.clone();
        let mut t_cluster = self.t_cluster.clone();
        for p in 0..self.num_places() {
            if p_cluster[p] == NO_CLUSTER {
                p_cluster[p] = c;
                let mut cluster: Vec<Place> = vec![];
                let mut stack: Vec<Place> = vec![p];
                while let Some(p) = stack.pop() {
                    cluster.push(p);
                    for &t in self.post_p(p) {
                        if t_cluster[t] == NO_CLUSTER {
                            t_cluster[t] = c;
                            for &p in self.pre_t(t) {
                                if p_cluster[p] == NO_CLUSTER {
                                    p_cluster[p] = c;
                                    stack.push(p);
                                }
                            }
                        }
                    }
                }
                self.clusters.push(cluster);
                c += 1;
            }
        }
        self.p_cluster = p_cluster;
        self.t_cluster = t_cluster;
    }

    pub fn check_free_choice(&self) -> Result<(), cli::CliError> {
        let col = fmt::Colorizer::new(fmt::ERR_COLORIZER);

        for c in &self.clusters {
            if let Some((&p1, ps)) = c.split_first() {
                let cpost = self.post_p(p1);
                for &p2 in ps {
                    if self.post_p(p2) != cpost {
                        let message = format!("Net is not free choice: places '{}' and '{}' have non-disjoint but different post-sets",
                                              col.warning(&self.places[p1]), col.warning(&self.places[p2]));
                        return Err(cli::CliError::with_message(message))
                    }
                }
            }
        }
        Ok(())
    }

    pub fn check_workflow_net(&self, multioutput: bool) -> Result<(), cli::CliError> {
        let col = fmt::Colorizer::new(fmt::ERR_COLORIZER);

        if self.p_input.len() == 0 {
            let message = format!("Net is not a workflow net: there is no input place");
            return Err(cli::CliError::with_message(message))
        }
        if self.p_output.len() == 0 {
            let message = format!("Net is not a workflow net: there is no output place");
            return Err(cli::CliError::with_message(message))
        }
        if self.p_input.len() >= 2 {
            let message = format!("Net is not a workflow net: both '{}' and '{}' are input places",
                                  col.warning(&self.places[self.p_input[0]]), col.warning(&self.places[self.p_input[1]]));
            return Err(cli::CliError::with_message(message))
        }
        if !multioutput && self.p_output.len() >= 2 {
            let message = format!("Net is not a workflow net: both '{}' and '{}' are output places",
                                  col.warning(&self.places[self.p_output[0]]), col.warning(&self.places[self.p_output[1]]));
            return Err(cli::CliError::with_message(message))
        }

        // forward DFS
        {
            let mut p_reach = vec![false; self.places.len()];
            let mut t_reach = vec![false; self.transitions.len()];
            for &i in &self.p_input {
                let mut stack = vec![i];
                p_reach[i] = true;
                while let Some(p) = stack.pop() {
                    for &t in self.post_p(p) {
                        if !t_reach[t] {
                            t_reach[t] = true;
                            for &p in self.post_t(t) {
                                if !p_reach[p] {
                                    p_reach[p] = true;
                                    stack.push(p);
                                }
                            }
                        }
                    }
                }
            }
            for p in 0..self.num_places() {
                if !p_reach[p] {
                    let message = format!("Net is not a workflow net: place '{}' is not reachable from an input place",
                                          col.warning(&self.places[p]));
                    return Err(cli::CliError::with_message(message))
                }
            }
            for t in 0..self.num_transitions() {
                if !t_reach[t] {
                    let message = format!("Net is not a workflow net: transition '{}' is not reachable from an input place",
                                          col.warning(&self.transitions[t]));
                    return Err(cli::CliError::with_message(message))
                }
            }
        }

        // backward DFS
        {
            let mut p_reach = vec![false; self.places.len()];
            let mut t_reach = vec![false; self.transitions.len()];
            for &o in &self.p_output {
                let mut stack = vec![o];
                p_reach[o] = true;
                while let Some(p) = stack.pop() {
                    for &t in self.pre_p(p) {
                        if !t_reach[t] {
                            t_reach[t] = true;
                            for &p in self.pre_t(t) {
                                if !p_reach[p] {
                                    p_reach[p] = true;
                                    stack.push(p);
                                }
                            }
                        }
                    }
                }
            }
            for p in 0..self.num_places() {
                if !p_reach[p] {
                    let message = format!("Net is not a workflow net: no output place reachable from place '{}'",
                                          col.warning(&self.places[p]));
                    return Err(cli::CliError::with_message(message))
                }
            }
            for t in 0..self.num_transitions() {
                if !t_reach[t] {
                    let message = format!("Net is not a workflow net: no output place reachable from transition '{}'",
                                          col.warning(&self.transitions[t]));
                    return Err(cli::CliError::with_message(message))
                }
            }
        }

        Ok(())
    }

    pub fn find_positive_p_invariant_for_clusters(&self) -> Result<PInvariant, cli::CliError> {
        match self.find_positive_p_invariant()? {
            Some(p_invariant) => {
                let mut p_invariant_cluster = vec![];
                for c in &self.clusters {
                    let mut count = 0.0;
                    for &p in c {
                        count += p_invariant[p];
                    }
                    p_invariant_cluster.push(count);
                }

                Ok(p_invariant_cluster)
            },
            None => return Err(cli::CliError::with_message("Could not find positive P-invariant for clusters")),
        }
    }

    pub fn get_concurrency_upper_bound(&self, lpfile: Option<&str>, use_integers: bool) -> Result<Count, cli::CliError> {
        let mut problem = LpProblem::new("Upper bound on concurrency threshold", LpObjective::Maximize);

        let mut p_vars_int: Vec<LpInteger> = vec![];
        let mut p_vars_cont: Vec<LpContinuous> = vec![];
        let mut t_vars_int: Vec<LpInteger> = vec![];
        let mut t_vars_cont: Vec<LpContinuous> = vec![];

        for p in 0..self.num_places() {
            let p_name = format!("p_{}", p);
            if use_integers {
                p_vars_int.push(LpInteger::lower_bound(&LpInteger::new(&p_name), 0.0));
            }
            else {
                p_vars_cont.push(LpContinuous::lower_bound(&LpContinuous::new(&p_name), 0.0));
            }
        }

        for t in 0..self.num_transitions() {
            let t_name = format!("t_{}", t);
            if use_integers {
                t_vars_int.push(LpInteger::lower_bound(&LpInteger::new(&t_name), 0.0));
            }
            else {
                t_vars_cont.push(LpContinuous::lower_bound(&LpContinuous::new(&t_name), 0.0));
            }
        }

        {
            let mut sum = LpExpression::EmptyExpr;
            for p in 0..self.num_places() {
                if use_integers {
                    sum = sum + (self.p_weight[p] as f32)*&p_vars_int[p];
                }
                else {
                    sum = sum + (self.p_weight[p] as f32)*&p_vars_cont[p];
                }
            }
            problem += sum;
        }

        let mut m0 = vec![0; self.num_places()];
        for &i in &self.p_input {
            m0[i] = 1;
        }

        for p in 0..self.num_places() {
            let mut sum = LpExpression::EmptyExpr;
            if use_integers {
                sum = sum + 1.0*&p_vars_int[p];
            }
            else {
                sum = sum + 1.0*&p_vars_cont[p];
            }
            for &t in self.pre_p(p) {
                if t != self.reset_transition {
                    if use_integers {
                        sum = sum - 1.0*&t_vars_int[t];
                    }
                    else {
                        sum = sum - 1.0*&t_vars_cont[t];
                    }
                }
            }
            for &t in self.post_p(p) {
                if t != self.reset_transition {
                    if use_integers {
                        sum = sum + 1.0*&t_vars_int[t];
                    }
                    else {
                        sum = sum + 1.0*&t_vars_cont[t];
                    }
                }
            }
            problem += sum.equal(m0[p]);
        }

        if let Some(file) = lpfile {
            problem.write_lp(file)?;
        }

        let solver = CbcSolver::new();

        let (status, res) = solver.run(&problem)?;
        match status {
            Status::Optimal => (),
            _ => return Err(cli::CliError::with_message("Could not solve system for upper bound on concurrency threshold")),
        };
        let mut val = 0;
        for p in 0..self.num_places() {
            let name = if use_integers { &p_vars_int[p].name } else { &p_vars_cont[p].name };
            val += self.p_weight[p]*(*res.get(name).unwrap_or(&0.0) as Count);
        }
        log!(LogLevel::Info, "Upper bound {} on concurrency threshold found", val);

        Ok(val)
    }

    pub fn find_positive_p_invariant(&self) -> Result<Option<PInvariant>, cli::CliError> {
        let mut problem = LpProblem::new("Minimal positive P-invariant", LpObjective::Minimize);

        let mut p_vars: Vec<LpContinuous> = vec![];
        for p in 0..self.num_places() {
            p_vars.push(LpContinuous::lower_bound(&LpContinuous::new(&self.places[p]), 1.0));
        }

        {
            let mut sum = LpExpression::EmptyExpr;
            for &p in &self.p_input {
                sum = sum + 1.0*&p_vars[p];
            }
            problem += sum;
        }

        for t in 0..self.num_transitions() {
            let mut sum = LpExpression::EmptyExpr;
            for &p in self.pre_t(t) {
                sum = sum + 1.0*&p_vars[p];
            }
            for &p in self.post_t(t) {
                sum = sum - 1.0*&p_vars[p];
            }
            problem += sum.equal(0.0);
        }

        let solver = CbcSolver::new();

        let (status, res) = solver.run(&problem)?;
        match status {
            Status::Optimal => (),
            Status::Infeasible => return Ok(None),
            Status::SubOptimal => return Ok(None),
            _ => return Err(cli::CliError::with_message("Could not solve system for positive P-invariant")),
        };
        let mut val = 0.0;
        for &p in &self.p_input {
            val += *res.get(&p_vars[p].name).unwrap();
        }
        let mut vals = vec![];
        for p in 0..self.num_places() {
            vals.push(*res.get(&p_vars[p].name).unwrap());
        }
        log!(LogLevel::Info, "Positive P-invariant with value {} found", val);
        Ok(Some(vals))
    }

    pub fn find_positive_t_invariant(&self) -> Result<Option<TInvariant>, cli::CliError> {
        let mut problem = LpProblem::new("Minimal positive T-invariant", LpObjective::Minimize);

        let mut t_vars: Vec<LpContinuous> = vec![];
        for t in &self.transitions {
            t_vars.push(LpContinuous::lower_bound(&LpContinuous::new(t), 1.0));
        }

        let ts = &t_vars[self.reset_transition];
        problem += ts;

        for p in 0..self.num_places() {
            let mut sum = LpExpression::EmptyExpr;
            for &t in self.pre_p(p) {
                sum = sum + 1.0*&t_vars[t];
            }
            for &t in self.post_p(p) {
                sum = sum - 1.0*&t_vars[t];
            }
            problem += sum.equal(0.0);
        }

        let solver = CbcSolver::new();

        let (status, res) = solver.run(&problem)?;
        match status {
            Status::Optimal => (),
            Status::Infeasible => return Ok(None),
            Status::SubOptimal => return Ok(None),
            _ => return Err(cli::CliError::with_message("Could not solve system for positive T-invariant")),
        };
        let val = *res.get(&ts.name).unwrap();
        let mut vals = vec![];
        for t in 0..self.num_transitions() {
            vals.push(*res.get(&t_vars[t].name).unwrap());
        }
        log!(LogLevel::Info, "Positive T-invariant with value {} found", val);
        Ok(Some(vals))
    }

    pub fn find_largest_unmarked_siphon(&self) -> Vec<Place> {
        let mut t_count = vec![0; self.num_transitions()];
        for t in 0..self.num_transitions() {
            t_count[t] = self.pre_t(t).len();
        }

        let mut p_siphon = vec![true; self.num_places()];
        let mut stack = vec![];

        for &p in &self.p_input {
            p_siphon[p] = false;
            stack.push(p);
        }

        while let Some(p) = stack.pop() {
            for &t in self.post_p(p) {
                t_count[t] -= 1;
                if t_count[t] == 0 {
                    for &p2 in self.post_t(t) {
                        if p_siphon[p2] {
                            p_siphon[p2] = false;
                            stack.push(p2);
                        }
                    }
                }
            }
        }

        let mut siphon = vec![];
        for p in 0..self.num_places() {
            if p_siphon[p] {
                siphon.push(p);
            }
        }

        siphon
    }

    pub fn find_rank_of_incidence_matrix(&self) -> Result<usize, cli::CliError> {
        let err_matrix = || cli::CliError::with_message("Could not build matrices for rank computation");

        let m = cmp::max(self.num_places(), self.num_transitions());
        let n = cmp::min(self.num_places(), self.num_transitions());
        let transpose = self.num_places() < self.num_transitions();

        let mut a = MatrixF64::new(m, n).ok_or_else(&err_matrix)?;
        for p in 0..self.num_places() {
            for &t in self.pre_p(p) {
                let (i, j) = if transpose { (t, p) } else { (p, t) };
                let val = a.get(i, j);
                a.set(i, j, val + 1.0);
            }
            for &t in self.post_p(p) {
                let (i, j) = if transpose { (t, p) } else { (p, t) };
                let val = a.get(i, j);
                a.set(i, j, val - 1.0);
            }
        }
        let mut s = VectorF64::new(n).ok_or_else(&err_matrix)?;
        let mut v = MatrixF64::new(n, n).ok_or_else(&err_matrix)?;
        let mut work = VectorF64::new(n).ok_or_else(&err_matrix)?;
        let result = SV_decomp(&mut a, &mut v, &mut s, &mut work);
        match result {
            Value::Success => (),
            _ => return Err(cli::CliError::with_message("Could not compute rank of incidence matrix")),
        }

        const TOLERANCE: f64 = 1e-12;

        let mut rank = 0;
        for i in 0..n {
            let val = s.get(i);
            if val.abs() > TOLERANCE {
                rank += 1;
            }
        }
        log!(LogLevel::Info, "Rank of incidence matrix: {}", rank);

        Ok(rank)
    }

    pub fn check_soundness(&self) -> Result<(), cli::CliError> {
        let col = fmt::Colorizer::new(fmt::ERR_COLORIZER);

        let p_invariant_opt = self.find_positive_p_invariant()?;
        if p_invariant_opt.is_none() {
            return Err(cli::CliError::with_message("Net is not sound: it has no positive P-invariant"))
        }

        let t_invariant_opt = self.find_positive_t_invariant()?;
        if t_invariant_opt.is_none() {
            return Err(cli::CliError::with_message("Net is not sound: it has no positive T-invariant"))
        }

        // Find largest unmarked siphon
        let siphon = self.find_largest_unmarked_siphon();
        if !siphon.is_empty() {
            let message = format!("Net is not sound: place '{}' is contained in an unmarked siphon",
                                  col.warning(&self.places[siphon[0]]));
            return Err(cli::CliError::with_message(message))
        }

        // Find rank of matrix
        let rank = self.find_rank_of_incidence_matrix()?;

        if rank + 1 != self.num_clusters() {
            let message = format!("Net is not sound: it has {} clusters but incidence matrix has a rank of {}",
                                  self.clusters.len(), rank);
            return Err(cli::CliError::with_message(message))
        }

        Ok(())
    }

    pub fn analyze(&self, multioutput: bool) -> Result<(), cli::CliError> {
        let col = fmt::Colorizer::new(fmt::OUT_COLORIZER);

        log!(LogLevel::Info, "Number of places: {}", self.num_places());
        log!(LogLevel::Info, "Number of transitions: {}", self.num_transitions());
        log!(LogLevel::Info, "Number of clusters: {}", self.num_clusters());
        self.check_workflow_net(multioutput)?;
        println!("Workflow net: {}", col.good("yes"));
        self.check_free_choice()?;
        println!("Free-choice net: {}", col.good("yes"));
        self.check_soundness()?;
        println!("Sound net: {}", col.good("yes"));
        Ok(())
    }

    pub fn is_marked_graph(&self) -> bool {
        for p in 0..self.num_places() {
            if self.pre_p(p).len() > 1 || self.post_p(p).len() > 1 {
                return false
            }
        }
        true
    }

    pub fn is_state_machine(&self) -> bool {
        for t in 0..self.num_transitions() {
            let pre = self.pre_t(t);
            if pre.len() > 0 {
                let pre_cluster = self.p_cluster[pre[0]];
                let pre_cluster_size = self.clusters[pre_cluster].len();
                let mut cur_size = 0;
                for &p in pre {
                    if self.p_cluster[p] != pre_cluster {
                        return false
                    }
                    else {
                        cur_size += 1;
                    }
                }
                if cur_size != pre_cluster_size {
                    return false
                }
            }
            let post = self.post_t(t);
            if post.len() > 0 {
                let post_cluster = self.p_cluster[post[0]];
                let post_cluster_size = self.clusters[post_cluster].len();
                let mut cur_size = 0;
                for &p in post {
                    if self.p_cluster[p] != post_cluster {
                        return false
                    }
                    else {
                        cur_size += 1;
                    }
                }
                if cur_size != post_cluster_size {
                    return false
                }
            }
        }
        true
    }

    pub fn is_acyclic(&self) -> bool {
        let mut stack = vec![];
        let mut p_count = vec![0; self.places.len()];
        let mut t_count = vec![0; self.transitions.len()];

        for p in 0..self.places.len() {
            p_count[p] = self.pre_p(p).len();
        }
        for t in 0..self.transitions.len() {
            t_count[t] = self.pre_t(t).len();
        }

        for &i in &self.p_input {
            p_count[i] = 0;
            stack.push(i);
        }

        let mut i = 0;

        while let Some(p) = stack.pop() {
            i += 1;
            for &t in self.post_p(p) {
                if t != self.reset_transition {
                    t_count[t] -= 1;
                    if t_count[t] == 0 {
                        for &p in self.post_t(t) {
                            p_count[p] -= 1;
                            if p_count[p] == 0 {
                                stack.push(p);
                            }
                        }
                    }
                }
            }
        }

        i == self.places.len()
    }

    pub fn get_concurrency_of_marked_graph(&self) -> Result<Count, cli::CliError> {
        self.get_concurrency_upper_bound(None, false)
    }

    pub fn get_concurrency_of_state_machine(&self) -> Count {
        let mut concurrency = 0;
        for c in &self.clusters {
            let mut c_weight = 0;
            for &p in c {
                c_weight += self.p_weight[p];
            }
            concurrency = cmp::max(concurrency, c_weight);
        }
        concurrency
    }

    pub fn num_places(&self) -> usize {
        self.places.len()
    }

    pub fn num_transitions(&self) -> usize {
        self.transitions.len()
    }

    pub fn num_clusters(&self) -> usize {
        self.clusters.len()
    }

    pub fn pre_p(&self, p: Place) -> &Vec<Transition> {
        &self.context_p[p].0
    }

    pub fn post_p(&self, p: Place) -> &Vec<Transition> {
        &self.context_p[p].1
    }

    pub fn pre_t(&self, t: Transition) -> &Vec<Place> {
        &self.context_t[t].0
    }

    pub fn post_t(&self, t: Transition) -> &Vec<Place> {
        &self.context_t[t].1
    }

    fn add_objects(net: pnml::Net, places: &mut Vec<String>, transitions: &mut Vec<(String, Vec<pnml::ToolSpecific>)>, arcs: &mut Vec<(String, String)>) {
        places.extend(net.places.into_iter().map(|p| p.id));
        transitions.extend(net.transitions.into_iter().map(|t| (t.id, t.toolspecific)));
        arcs.extend(net.arcs.into_iter().map(|a| (a.source, a.target)));
        for page in net.pages {
            PetriNet::add_objects(page, places, transitions, arcs);
        }
    }

    pub fn try_from(pnml: pnml::PNML, multioutput: bool, unweighted: bool) -> Result<PetriNet, cli::CliError> {
        let mut places: Vec<String> =  vec![];
        let mut transitions: Vec<(String, Vec<pnml::ToolSpecific>)> = vec![];
        let mut arcs: Vec<(String, String)> = vec![];
        PetriNet::add_objects(pnml.net, &mut places, &mut transitions, &mut arcs);
        PetriNet::with_arcs(places, transitions, arcs, multioutput, unweighted)
    }

    pub fn get_cluster(&self, c: Cluster) -> ClusterObj {
        ClusterObj { net: self, c: c }
    }

    pub fn print_information(&self) {
        let col = fmt::Colorizer::new(fmt::OUT_COLORIZER);
        let col_result = |r: bool| if r { col.good("yes") } else { col.error("no") };

        println!("Marked graph: {}", col_result(self.is_marked_graph()));
        println!("State machine: {}", col_result(self.is_state_machine()));
        println!("Acyclic: {}", col_result(self.is_acyclic()));
        println!("Number of places: {}", self.num_places());
        println!("Number of transitions: {}", self.num_transitions() - 1);
    }
}

impl Display for PetriNet {
    fn fmt(&self, f: &mut std_fmt::Formatter) -> std_fmt::Result {
        write!(f, "places {{")?;
        for p in 0..self.num_places() {
            write!(f, " {} (w: {})", self.places[p], self.p_weight[p])?;
        }
        writeln!(f, " }}")?;

        write!(f, "transitions {{")?;
        for t in 0..self.num_transitions() {
            write!(f, " {} (w: {}, t: {})", self.transitions[t], self.t_weight[t], self.t_time[t])?;
        }
        writeln!(f, " }}")?;

        writeln!(f, "place arcs {{")?;
        for p in 0..self.num_places() {
            let pre = self.pre_p(p);
            let post = self.post_p(p);

            if pre.len() > 0 || post.len() > 0 {
                write!(f, "   ")?;

                if pre.len() > 0 {
                    if pre.len() > 1 {
                        write!(f, "{{ ")?;
                    }
                    for &t in pre {
                        write!(f, "{} ", self.transitions[t])?;
                    }
                    if pre.len() > 1 {
                        write!(f, "}} ")?;
                    }
                    write!(f, "-> ")?;
                }

                write!(f, "{} ", self.places[p])?;

                if post.len() > 0 {
                    write!(f, "-> ")?;
                    if post.len() > 1 {
                        write!(f, "{{ ")?;
                    }
                    for &t in post {
                        write!(f, "{} ", self.transitions[t])?;
                    }
                    if post.len() > 1 {
                        write!(f, "}} ")?;
                    }
                }
                writeln!(f, "")?;
            }
        }
        writeln!(f, "}}")?;

        writeln!(f, "transition arcs {{")?;
        for t in 0..self.num_transitions() {
            let pre = self.pre_t(t);
            let post = self.post_t(t);

            if pre.len() > 0 || post.len() > 0 {
                write!(f, "   ")?;

                if pre.len() > 0 {
                    if pre.len() > 1 {
                        write!(f, "{{ ")?;
                    }
                    for &p in pre {
                        write!(f, "{} ", self.places[p])?;
                    }
                    if pre.len() > 1 {
                        write!(f, "}} ")?;
                    }
                    write!(f, "-> ")?;
                }

                write!(f, "{} ", self.transitions[t])?;

                if post.len() > 0 {
                    write!(f, "-> ")?;
                    if post.len() > 1 {
                        write!(f, "{{ ")?;
                    }
                    for &p in post {
                        write!(f, "{} ", self.places[p])?;
                    }
                    if post.len() > 1 {
                        write!(f, "}} ")?;
                    }
                }
                writeln!(f, "")?;
            }
        }
        writeln!(f, "}}")?;

        write!(f, "input places {{")?;
        for &p in &self.p_input {
            write!(f, " {}", self.places[p])?;
        }
        writeln!(f, " }}")?;

        write!(f, "output places {{")?;
        for &p in &self.p_output {
            write!(f, " {}", self.places[p])?;
        }
        writeln!(f, " }}")?;

        write!(f, "clusters {{ ")?;
        for c in &self.clusters {
            if c.len() > 1 {
                write!(f, "{{ ")?;
            }
            for &p in c {
                write!(f, "{} ", self.places[p])?;
            }
            if c.len() > 1 {
                write!(f, "}} ")?;
            }
        }
        writeln!(f, "}}")?;

        write!(f, "place clusters {{")?;
        for (p, &c) in self.p_cluster.iter().enumerate() {
            if c == NO_CLUSTER {
                write!(f, " {}(-)", self.places[p])?;
            }
            else {
                write!(f, " {}({})", self.places[p], c)?;
            }
        }
        writeln!(f, " }}")?;

        write!(f, "transitions clusters {{")?;
        for (t, &c) in self.t_cluster.iter().enumerate() {
            if c == NO_CLUSTER {
                write!(f, " {}(-)", self.transitions[t])?;
            }
            else {
                write!(f, " {}({})", self.transitions[t], c)?;
            }
        }
        writeln!(f, " }}")?;

        Ok(())
    }
}
