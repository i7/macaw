/**
 * Macaw - Compute bounds on the concurrency threshold of a workflow net
 *
 *  Copyright 2018 by Philipp Meyer <meyerphi@in.tum.de>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See LICENSE.md and README.md.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

use std::fmt::Display;
use std::str::FromStr;
use std::collections::HashMap;
use std::default::Default;

use serde::de::{self, Deserialize, Deserializer};

#[derive(Deserialize, Debug)]
pub struct PNML {
    pub net: Net
}

#[derive(Deserialize, Debug)]
pub struct Net {
    pub id: String,
    #[serde(rename="page", default)]
    pub pages: Vec<Net>,
    #[serde(rename="place", default)]
    pub places: Vec<Place>,
    #[serde(rename="transition", default)]
    pub transitions: Vec<Transition>,
    #[serde(rename="arc", default)]
    pub arcs: Vec<Arc>,
}

#[derive(Deserialize, Debug)]
pub struct Place {
    pub id: String,
    #[serde(rename="initialMarking", default)]
    pub initial_marking: Marking,
}

#[derive(Deserialize, Debug)]
pub struct Transition {
    pub id: String,
    #[serde(default)]
    pub toolspecific: Vec<ToolSpecific>,
}

#[derive(Deserialize, Debug)]
pub struct ToolSpecific {
    pub tool: String,
    #[serde(rename="property", default, deserialize_with = "from_list")]
    pub properties: HashMap<String, String>,
}

#[derive(Deserialize, Debug)]
pub struct Arc {
    pub id: String,
    pub source: String,
    pub target: String,
}

#[derive(Deserialize, Eq, PartialEq, Default, Debug)]
pub struct Marking {
    #[serde(rename="text", default, deserialize_with = "from_str")]
    pub value: u32,
}

fn from_str<'de, T, D>(deserializer: D) -> Result<T, D::Error>
    where T: FromStr,
          T::Err: Display,
          D: Deserializer<'de>
{
    let s = String::deserialize(deserializer)?;
    T::from_str(&s).map_err(de::Error::custom)
}

#[derive(Deserialize, Debug)]
pub struct Item<T>
    where T: FromStr,
          T: Default,
          T::Err: Display
{
    pub key: String,
    #[serde(rename="$value", default, deserialize_with = "from_str")]
    pub value: T,
}

fn from_list<'de, T, D>(deserializer: D) -> Result<HashMap<String, T>, D::Error>
    where T: FromStr,
          T: Default,
          T::Err: Display,
          D: Deserializer<'de>
{
    let mut map = HashMap::new();
    for item in Vec::<Item<T>>::deserialize(deserializer)? {
        map.insert(item.key, item.value);
    }
    Ok(map)
}
