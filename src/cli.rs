/**
 * Macaw - Compute bounds on the concurrency threshold of a workflow net
 *
 *  Copyright 2018 by Philipp Meyer <meyerphi@in.tum.de>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See LICENSE.md and README.md.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

use std::fmt as std_fmt;
use std::fmt::Display;
use std::io;
use std::convert;
use std::process;
use std;

use num_rational;
use serde_xml_rs;
use log;

use fmt;

#[derive(Debug)]
pub struct CliError {
    message: String,
}

impl CliError {
    pub fn with_message<D>(x: D) -> CliError where D: Display {
        let c = fmt::Colorizer::new(fmt::ERR_COLORIZER);
        CliError { message: format!("{} {}", c.error("error:"), x) }
    }
}

impl Display for CliError {
    fn fmt(&self, f: &mut std_fmt::Formatter) -> std_fmt::Result {
        writeln!(f, "{}", self.message)
    }
}

impl CliError {
    /// Prints the error to `stderr` and exits with a status of `1`
    pub fn exit(&self) -> ! {
        use std::io::{Write, stderr};
        writeln!(&mut stderr(), "{}", self.message).ok();
        process::exit(1);
    }
}

impl convert::From<io::Error> for CliError {
    fn from(err: io::Error) -> CliError {
        CliError::with_message(format!("I/O error: {}", err))
    }
}

impl convert::From<serde_xml_rs::Error> for CliError {
    fn from(err: serde_xml_rs::Error) -> CliError {
        CliError::with_message(format!("Could not parse input file: {}", err))
    }
}

impl convert::From<log::SetLoggerError> for CliError {
    fn from(err: log::SetLoggerError) -> CliError {
        CliError::with_message(format!("Could not set up logger: {}", err))
    }
}

impl convert::From<num_rational::ParseRatioError> for CliError {
    fn from(err: num_rational::ParseRatioError) -> CliError {
        CliError::with_message(format!("Could not parse rational: {}", err))
    }
}

impl convert::From<std::num::ParseIntError> for CliError {
    fn from(err: std::num::ParseIntError) -> CliError {
        CliError::with_message(format!("Could not parse integer: {}", err))
    }
}

impl convert::From<String> for CliError {
    fn from(err: String) -> CliError {
        CliError::with_message(format!("Error solving linear program: {}", err))
    }
}
